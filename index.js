// console.log("hello asis")

// [Section] - Functions
// Functions in JS are lines/blocks of codes that will tell our device/application to per
/*

Syntax:
Function functionName()



function calcAge1(birthYear) {
  return 2023 - birthYear;
}
const age1 = calcAge1(1998);
console.log(age1);
*/

function printGrade() {
  let grade2 = 95;
  let grade1 = 90;
  let ave = (grade1 + grade2) / 2

  console.log(ave);
}
printGrade();
printGrade();

// Function invocation
// Invocation will execute block of codes inside the function being called.

// [Section] Function Declaration VS Expression

function declaredFunction() {
  console.log("asis")
}
declaredFunction();

// variableFunction();
/*

  error - function expressions being stored 
  in a let or const, cannot be hoisted.

*/
// anonymous function
// Function Expression example

let variableFunction = function () {
  console.log("hello....");
}
variableFunction();

let functionExpression = function funcName() {
  console.log("hello po");
}
functionExpression();

// you can re-assign declared functions and function expression to a new anonymous function.

declaredFunction = function () {
  console.log("Updated declaredFunction");
}
declaredFunction();

functionExpression = function () {
  console.log("updated");
}
functionExpression();

// however, we cannot re-assign a function expression initialize with const. 

// [Section] Function Scoping 
/*
local/Block Scope
Global Scope
function scope

*/

{
  let locaVariable = "Kyle Luis";
  console.log(locaVariable);
}
let globalVariable = "mr. Luis";
console.log(globalVariable);

function showNames() {
  // Function Scope Variables
  var functionVar = "asis";
  const functionConst = "Kyle";
  let functionLet = "eh";
  console.log(functionVar);
  console.log(functionConst);
  console.log(functionLet);
}
showNames();

// Nested Function

function myNewFunction() {
  let name = "Asis";

  function nestedFunction() {
    let nestedName = "Kyle";
    console.log(name);
  }
  nestedFunction();
  // console.log(nestedName); -> Error/Function Scope.
}
myNewFunction();
// nestedFunction(); -> error its inside a parent scope.

let globalName = "jen";

function myNewFunction2() {
  let nameInside = "asis"
  console.log(globalName);
  console.log(nameInside);
}
myNewFunction2();

// [Section] - Using Alert (prompt)

// alert("hello asis");

// function alertFunction() {
//   alert("hello");
// }
// alertFunction();

//  notes on the user of alert()
// show only alert() for short dialog message.

// [Section] using prompt
// prompt() allows us to show a small window and gather user input.

// let samplePrompt = prompt("enter your name.");

// console.log("hello, " + samplePrompt);
// console.log(typeof samplePrompt);

// function printWelcomeMessage() {
//   let firstName = prompt("First Name");
//   let lastName = prompt("Last Name");

//   console.log("hello " + firstName + " " + lastName + "!");
//   console.log("welcome sa page ko po!");
// }
// printWelcomeMessage();

function getCourse() {
  let courses = ["science 101", "math 101", "english 101"];
  console.log(courses);
}
getCourse();


function get() {
  let name = "jayme";
  console.log(name);
}
get();